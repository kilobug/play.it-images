#!/bin/sh
set +e

if [ $# != 2 ] ; then
	echo 'USAGE: add-game-screenshot.sh $game_id $source_image'
	exit 1
fi

game_id="$1"
source_file="$2"

screenshots_directory="$(readlink --canonicalize-existing "$(dirname "$0")")/../games/$game_id"

mkdir --parents "$screenshots_directory"
convert "$source_file" "$screenshots_directory/screenshot.jpg"
convert "$source_file" -resize 320 "$screenshots_directory/thumbnail.jpg"

exit 0
